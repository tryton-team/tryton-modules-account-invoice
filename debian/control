Source: tryton-modules-account-invoice
Section: python
Priority: optional
Maintainer: Debian Tryton Maintainers <team+tryton-team@tracker.debian.org>
Uploaders: Mathias Behrle <mathiasb@m9s.biz>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               jdupes,
               python3-all,
               python3-setuptools,
               python3-sphinx
Standards-Version: 4.7.0
Homepage: https://www.tryton.org/
Vcs-Git: https://salsa.debian.org/tryton-team/tryton-modules-account-invoice.git
Vcs-Browser: https://salsa.debian.org/tryton-team/tryton-modules-account-invoice
Rules-Requires-Root: no

Package: tryton-modules-account-invoice
Architecture: all
Depends: python3-dateutil,
         python3-sql,
         tryton-modules-account (>= ${version:major}),
         tryton-modules-account-product (>= ${version:major}),
         tryton-modules-company (>= ${version:major}),
         tryton-modules-currency (>= ${version:major}),
         tryton-modules-party (>= ${version:major}),
         tryton-modules-product (>= ${version:major}),
         tryton-server (>= ${version:major}),
         ${API},
         ${misc:Depends},
         ${python3:Depends},
         ${sphinxdoc:Depends}
Description: Tryton application platform - account invoice module
 Tryton is a high-level general purpose application platform. It is the base
 of a complete business solution as well as a comprehensive health and hospital
 information system (GNUHealth).
 .
 This package contains the financial and accounting module with:
 .
  * Payment Term
  * Invoice/Credit Note
  * Supplier Invoice/Supplier Credit Note
 .
 With the possibilities:
 .
  * to follow the payment of the invoices.
  * to define invoice sequences on fiscal year or period.
  * to credit any invoice.
